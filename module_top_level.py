# -*- coding: utf-8 -*- 
'''
 * Copyright (C) 2016 Jaroslav Kopáček
 *
 * This file is part of Linux Driver Generator.
 *
 * Linux Driver Generator is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Linux Driver Generator is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Linux Driver Generator. If not, see <http://www.gnu.org/licenses/>.
'''
import pprint
import re
import datetime
from driver_structs import *
from variable import *
from probe import *
from remove import *

'''
  Class generating top level variables and functions.
  @class ModuleTopLevel
  @brief Class generating top level variables and functions.
'''
class ModuleTopLevel:
  '''
    Constructor
    @fn __init__
    @param moduleInfo information about module
    @param moduleDef definitions in module
    @param moduleFeatures features
    @brief Class constructor which sets default values. Constructor sets \
    default include directives, define directives, file comments, global \
    variables (DriverItems, file_operations, of_device_id, platform_driver), \
    functions (defined in file_operations, platform_driver [probe, remove]).
  '''
  def __init__ (self, moduleInfo, moduleDef, moduleFeatures, procfsFlag = False, sysfsFlag = False):
    self.moduleLicenses = frozenset (["GPL", "GPL v2", "GPL and additional rights", "Dual BSD/GPL", "Dual MIT/GPL", "Dual MPL/GPL", "Proprietary"])
    
    # DATA FROM PARSER
    self.moduleDef = moduleDef
    self.moduleInfo = moduleInfo
    self.moduleFeatures = moduleFeatures

    # DEFAULT SET OF NEEDED HEADER FILES
    self.include = set ()
    self.__setIncludes ()
    
    # DEFAULT SET OF NEEDED DEFINE DIRECTIVES
    self.define = dict ()
    self.__setDefines ()
    
    self.kernelMacros = dict ()
    
    # COMMENTS ABOUT FILE
    self.comments = dict ()
    self.__setAboutModule ()

    # DEFAULT SET OF NEEDED STRUCTS
    self.globalVariables = dict ()
    self.__setGlobalVariables ()
    
    # DEFAULT SET OF NEEDED FUNCTIONS (functions defined in file_operations, \
    # probe and remove functions defined in platform_driver)
    self.functions = dict ()
    self.__setOperations ()
    
    # FLAGS REFLECTING REQUIRMENTS FOR VIRTUAL FS
    self.procfsFlag = procfsFlag
    self.sysfsFlag = sysfsFlag
    
  '''
    Default operations
    @fn __setOperations
    @brief Function sets default functions defined in file_operations \
    structure and probe and remove functions defined in platform_driver \
    structure.
  '''
  def __setOperations (self):
    # GENERATE DRIVER OPERATIONS
    for item in self.moduleFeatures:
      if item["exec"] == "exit":
        self.functions ["remove"] = RemoveFunction (self, item["variables"], item["code"])
      if item["exec"] == "init":
        self.functions ["probe"] = ProbeFunction (self, item["variables"], item["code"])

    if "remove" not in self.functions:
      self.functions ["remove"] = RemoveFunction (self)
    if "probe" not in self.functions:
      self.functions ["probe"] = ProbeFunction (self)
      
    # GENERATE DEVICE FILE OPERATIONS
    self.globalVariables["dev_fops"].generateFunctions (self)
    
  '''
    Default include directives
    @fn __setIncludes
    @brief Function sets default include directives.
  '''
  def __setIncludes (self):
    # NEEDED HEADER FILES GENERALLY FOR MODULE DRIVER
    self.addHeaderFile ("linux/init.h")
    self.addHeaderFile ("linux/module.h")
    self.addHeaderFile ("linux/kernel.h")
    # NEEDED HEADER FILES FOR PROBING AND REMOVING
    self.addHeaderFile ("linux/cdev.h")
    self.addHeaderFile ("asm/io.h")
    self.addHeaderFile ("linux/of_address.h")
    self.addHeaderFile ("linux/platform_device.h") # platform_driver
    self.addHeaderFile ("linux/ioport.h")          # struct resouce
    self.addHeaderFile ("linux/slab.h")            # kmalloc
    self.addHeaderFile ("linux/fs.h")              # alloc_chrdev_region

  '''
    Add header file
    @fn addHeaderFile
    @brief Function adds header file into set.
  '''
  def addHeaderFile (self, file):
    self.include.add ("#include <" + file + ">\n")
     
  '''
    Add own header file
    @fn addOwnHeaderFile
    @brief Function adds own header file into set.
  '''
  def addOwnHeaderFile (self, file):
    self.include.add ("#include \"" + file + "\"\n")

  '''
    Generate code cantaining include directives
    @fn generateIncludeDirectives
    @return code
    @brief Generate code cantaining include directives
  '''
  def generateIncludeDirectives (self):
    code = ""
    
    for directive in self.include:
      code += directive
      
    code += "\n"
    
    return code

  '''
    Default define directives
    @fn __setDefines
    @brief Function sets default defines directives.
  '''
  def __setDefines (self):
    self.addDefine ("MODULE_NAME", self.moduleDef["module_name"], "moduleName")
    self.addDefine ("DRIVER_NAME", self.moduleDef["driver_name"], "driverName")

  '''
    Add define directive
    @fn addDefine
    @brief Function adds define directive.
  '''
  def addDefine (self, name, value, key = None):
    if key:
      self.define [key] = {"code":"#define " + name + " \"" + value + "\"\n", "name":name, "value":value}

    else:
      self.define [name] = {"code":"#define " + name + " \"" + value + "\"\n", "name":name, "value":value}

  '''
    Generate code cantaining define directives
    @fn generateDefineDirectives
    @return code
    @brief Generate code cantaining define directives
  '''
  def generateDefineDirectives (self):
    code = ""
    
    for define in self.define.itervalues():
      code += define["code"] + '\n'
  
    code += '\n'
    
    return code

  '''
    Default comments about module
    @fn __setAboutModule
    @brief Function sets default comments about module.
  '''
  def __setAboutModule (self):
    fileHeaderComment = ""

    fileHeaderComment += "/**\n"
    fileHeaderComment += (" * @file " + self.moduleDef["module_name"] + ".c \n")
    
    chunks, chunk_size = len(self.moduleInfo ["description"]), 80
    if chunks > chunk_size:
      fileHeaderComment += " *\n"
      for i in range(0, chunks, chunk_size):
        fileHeaderComment += " * " + self.moduleInfo ["description"] [i:i+chunk_size] 
      fileHeaderComment += " \n *\n"
      
    else:
      fileHeaderComment += " *\n"
      fileHeaderComment += " * " + self.moduleInfo ["description"] 
      fileHeaderComment += " \n *\n"      
      
    fileHeaderComment += (" * @author " + self.moduleInfo ["author"] + "\n")
    
    #if self.moduleInfo ["date"]:
      #fileHeaderComment += (" * @created " + self.moduleInfo ["date"] + "\n")

    fileHeaderComment += (" * @created " + str (datetime.date.today ()) + "\n")

    fileHeaderComment += " *\n *\n"      
    fileHeaderComment += " * Generated by Linux driver generator developed as bachalor's thesis project.\n" 
    fileHeaderComment += " */\n"

    self.comments ["top"] = fileHeaderComment
    self.comments ["bottom"] = ("/***                     End of file " + self.moduleDef["module_name"] + ".c                     ***/")
    
    
    # MODEULE INFO PART
    self.addHeaderFile ("linux/module.h")

    self.kernelMacros ["author"] = "MODULE_AUTHOR (\"" + self.moduleInfo["author"] + "\");"
    
    if (self.moduleInfo["license"] in self.moduleLicenses):
      self.kernelMacros ["license"] = "MODULE_LICENSE (\"" + self.moduleInfo["license"] + "\");"
  
    else:
      raise ValueError ("Unsupported module license.")    

  '''
    Default global variables and structures
    @fn __setGlobalVariables
    @brief Function sets default global variables and structures.
  '''
  def __setGlobalVariables (self):
    # OWN DEVICE STRUCT
    self.globalVariables["driver_items"]  = DriverItems ()

    # INSTANCE OF OWN DEVICE STRUCT
    self.globalVariables["driver_instance"] = DriverItems (True)
    
    # DEVICE MATCH STRUCT
    self.globalVariables["dev_match"] = OfDeviceId (self.moduleDef ["compatible"])
    
    # PLATFORM DRIVER STRUCTURE
    self.globalVariables["platform_driver"] = PlatformDriver ()
    
    # DEVICE FILE OPERATIONS STRUCT
    self.globalVariables["dev_fops"] =  FileOperations ()
    
  '''
    Add global variable
    @fn addGlobalVariable
    @brief Function adds global variable into set.
  '''
  def addGlobalVariable (self, type, identificator):
    self.globalVariables [identificator] = {"code":type + " " + identificator + ";", "identificator":identificator, "type":type}
    
  '''
    Generate module_init macro
    @fn generateInit
    return code
    @brief Function generates generate module_init macro.
  '''
  def generateInit (self):
    identifier = self.functions["probe"].identifier + "_init"  

    self.kernelMacros ["module_init"] = "module_init (" + identifier + ");"

    code = ""
    
    code += "static int __init " + identifier +" (void) {\n"

    code += "\treturn platform_driver_register (&" + self.globalVariables["platform_driver"].identifier + ");\n"
    
    code += "}\n"

    return code
  
  '''
    Generate module_exit macro
    @fn generateInit
    return code
    @brief Function generates generate module_exit macro.
  '''
  def generateExit (self):
    identifier = self.functions["remove"].identifier + "_exit"

    self.kernelMacros ["module_exit"] = "module_exit (" + identifier + ");"

    code = ""
    
    code += "static void __exit " + identifier +" (void) {\n"

    code += "\tplatform_driver_unregister (&" + self.globalVariables["platform_driver"].identifier + ");\n"
    
    code += "}\n"

    return code

  def procfsSupport (self):
    self.addHeaderFile ("linux/proc_fs.h")
    
    #self.globalVariables["proc_dir_entry"] = Procfs ()
    # PROCFS FILE OPERATIONS STRUCT
    self.globalVariables["proc_fops"] =  ProcFileOperations ()
    self.globalVariables["proc_fops"].generateFunctions (self)
    
    #self.getProcRead ()
    #self.getProcWrite ()
    
    
    
  def getProcRead (self):
    for feature in  self.moduleFeatures:
      if feature["exec"] == "proc_read":
        code = "int proc_read( char *page, char **start, off_t off, int count, int *eof, void *data ) {\n"
        
        for var in feature["variables"]:
          code += var.generateDefinition() + "\n"

        code += "\n"
        
        code += feature["code"] + "\n"

        code += "}\n"
        
        self.functions ["proc_read"] = code;
      
  def getProcWrite (self):
    for feature in  self.moduleFeatures:
      if feature["exec"] == "proc_write":
        code = "int proc_write( struct file *filp, const char __user *buff, unsigned long len, void *data ) {\n"
        
        for var in feature["variables"]:
          code += var.generateDefinition() + "\n"

        code += "\n"
        
        code += feature["code"] + "\n"

        code += "}\n"
        
        self.functions ["proc_write"] = code;
      
    






