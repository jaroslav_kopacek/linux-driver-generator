import warnings
import pprint
import re


class Module:

  def __init__ (self):
    self.module_licenses = frozenset (["GPL", "GPL v2", "GPL and additional rights", "Dual BSD/GPL", "Dual MIT/GPL", "Dual MPL/GPL", "Proprietary"])
#    self.module_info_output = dict ();
    self.module_def_output = dict ();
    self.module_globals = dict ()
    self.headers = set ()
    self.init_body = ""

    
  def add_header_file (self, include_string):
    self.headers.add (include_string)


  def create_about (self, author, description, date):
    about = "/** About module: \n"
    chunks, chunk_size = len(description), 80
    for i in range(0, chunks, chunk_size):
      about += " * " + description [i:i+chunk_size] 
    about += ("\n * Author(s): " + author + "\n")
    about += ("\n * Created: " + date + "\n")
    about += ("\n * Created by driver generator\n*/")
    return about

  def create_comment (self, description, multiline = False):
    print "given desc:\n" + description
    
    comment = ""
    chunks, chunk_size = len(description), 80
    if multiline and chunks > chunk_size:
      comment += "/*\n"
      for i in range(0, chunks, chunk_size):
        comment += " * " + description [i:i+chunk_size] 
      comment += " */\n"
      
    elif multiline and chunks < chunk_size:
      comment += "/*\n"
      comment += " * " + description 
      comment += " */\n"
      
    else:
      comment += "// " + description
      
    return comment + '\n'

    # NOTE Firstly process module def subblocks
    
  def create_condition (self, op1, op2, operator, assignments = False, op3 = None):
    if assignments and op3:
      return "if ((" + op1 + " = " + op2 + ") " + operator + " "  + op3 + ")"
  
    elif not assignments and not op3:
      return "if (" + op1 + " " + operator + " "  + op2 + ")"
    
    else:
      raise Exception ("(create_condition) Bad params combination.")
    
  def create_printk (*arg):
    args = ""
    for i in range (3, len (arg)):
      args += ', ' +str (arg [i])

    return "printk (" + arg [1] + " \"" + arg [2] + "\"" + args +");"
    
    
    
  '''
  
  '''
  def create_globals (self, module_info, module_def):
    # MODEULE INFO PART
    self.add_header_file ("linux/module.h")
    for param, value in module_info.items ():
      if (param == "author"):
        self.module_globals ["M_author"] = "MODULE_AUTHOR (\"" + value + "\");"
        
      elif (param == "license"):
        if (value in self.module_licenses):
          self.module_globals ["M_license"] = "MODULE_LICENCE (\"" + value + "\");"
            
        else:
          raise ValueError ("Unsupported module license.")
        
      #elif (param == "description"):
        #self.module_info_output ["description"] = value
        #print "Param description processed" 
        
      #elif (param == "date"):
        #self.module_info_output ["date"] = value
        #print "Param date processed" 
        
      #else:
        #warnings.warn("Unsupported module param (" + param + ": " + value + "). Param skipped.")
          
    # Generate comment about module
    self.module_globals['C_about_module'] = self.create_about (module_info ['author'], module_info ['description'], module_info ['date'])

    
    # MODEULE DEFINITION PART
    self.module_globals ['D_dev_name'] = "#define DEVNAME \"" + module_def['dev_name'] + "\""
    
    self.add_header_file ("linux/types.h")
    self.module_globals ['V_dev_num'] = "dev_t dev_num;"

    self.add_header_file("linux/cdev.h")
    self.module_globals ['P_cdev'] = "struct cdev *" + module_def['dev_name'] +"_cdev;"

    self.add_header_file ("linux/init.h")
    for param, value in module_def.items ():
      if param == "dev_name":
        self.module_globals ["proto_init"] = "static int __init " + value.replace (' ', '_') + "_init ();"
        self.module_globals ["proto_exit"] = "static void __exit " + value.replace (' ', '_') + "_exit ();"
        self.module_globals ["M_init"] = "module_init (\"" + value.replace (' ', '_') + "\");"
        self.module_globals ["M_exit"] = "module_exit (\"" + value.replace (' ', '_') + "\");"


    ### TODO NOTE Place for global register addresses (V) and offsets (D)
    
    print "--------------------------------------------------\nModule.create_globals () - module_globals:"
    pprint.pprint (self.module_globals)
    
    
  
  def create_init (self, module_def_subblocks):
    self.init_body += self.module_globals ["proto_init"][:-1] + ' {\n'

    ### INITIAL PROCEDURE FOR CDEVs
    self.init_body += "int state;"
    self.init_body += self.create_comment ("device registration")
    self.init_body += self.create_condition ("state", "alloc_chrdev_region (&" + self.module_globals["V_dev_num"][6:-1] + ", 0, 1, " + self.module_globals["D_dev_name"][8:15] + ")", "!=", True, "0")
    self.init_body += self.create_printk ("KERN_ALERT", "failed to register a region dynamically\n")
    self.init_body += "else\n"
    self.init_body += self.create_printk ("KERN_INFO", "major number = %d\n", "MAJOR(" + self.module_globals["V_dev_num"][6:-1] + ")")
    self.init_body += self.module_globals["P_cdev"][13:30] + " = cdev_alloc ();"
    self.init_body += self.module_globals["P_cdev"][13:30] + "->owner = THIS_MODULE;"
    self.init_body += "state = cdev_add (" + self.module_globals["P_cdev"][13:30] + ", " + self.module_globals["D_dev_name"][8:15] + ", 1);"

    #### USER SPECIFIED PART
    #for item in module_def_subblocks:
      #if item['type'] == "feature" and item ['exec'] == "init":
        #if item ["description"]:
          #self.init_body += self.create_comment (item ["description"], True)
        
        
        
        #self.init_body += item ["code"]

    #### END OF USER SPECIFIED PART
    
    
    
    self.init_body += "\n}"
    
    print "--------------------------------------------------\nModule.create_init () - init_body:"
    pprint.pprint (self.init_body)
        
    
  def parse_feature (self, block):
    print "parse_feature"
    while True:
      line = block.split ()
    result = re.match ("^", re)
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
