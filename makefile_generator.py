# -*- coding: utf-8 -*- 
'''
 * Copyright (C) 2016 Jaroslav Kopáček
 *
 * This file is part of Linux Driver Generator.
 *
 * Linux Driver Generator is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Linux Driver Generator is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Linux Driver Generator. If not, see <http://www.gnu.org/licenses/>.
'''

'''
  Class generating Makefile.
  @class Makefile
  @brief Class generating Makefile based on generator parameters.
'''
class Makefile:
  '''
    Constructor
    @fn __init__
    @param fileName output file name
    @param kernSrc path to kernel source
    @param cc path to crosscompile toolchain
    @param flags array of CFLAGS for compiler
    @brief Class constructor which sets default values.
  '''
  def __init__ (self, fileName, kernSrc = "", cc = "", flags = None):
    self.code = ""
    self.fileName = fileName
    self.kernSrc = kernSrc
    self.flags = flags
    self.cc = cc
  
  '''
    Generates params for Makefile.
    @fn params
    @brief Method generating parameters for Makefile.
  '''
  def params (self):
    code = ""
    
    code += "obj-m += " + self.fileName + ".o\n"
    
    code += "KERN_SRC = " + self.kernSrc + "\n"
    
    code += "PWD = $(shell pwd)\n\n"
    
    code += "include $(KERN_SRC)/.config\n\n"
    
    code += "CFLAGS = -D__KERNEL__ -DMODULE -I$(KERN_SRC)/include -O -Wall\n" 
    
    if self.flags:
      for flag in self.flags:
        code += " " + flag
      
      code += "\n"
    
    return code
    
  '''
    Generates target "all"..
    @fn targetAll
    @brief Method generating target "all".
  '''
  def targetAll (self):
    code = ""
    
    code = "all:\n"
    
    code += "\tKBUILD_NOPEDANTIC=1 make ARCH=arm"
    
    if self.cc != "":
      code += " CROSS_COMPILE=\"" + self.cc + "\""
    
    code += " -C $(KERN_SRC) SUBDIRS=$(PWD) M=$(PWD) modules\n"
    
    return code
    
  '''
    Generates target "clean"..
    @fn targetClean
    @brief Method generating target "clean".
  '''
  def targetClean (self):
    code = ""
    
    code = "clean:\n"
    
    code += "\tKBUILD_NOPEDANTIC=1 make ARCH=arm "
    
    if self.cc != "":
      code += "CROSS_COMPILE=\"" + self.cc + "\""
    
    code += " -C $(KERN_SRC) SUBDIRS=$(PWD) M=$(PWD) clean\n"
    
    return code
    
  '''
    Generates Makefile's code.
    @fn targetAll
    @brief Method generating Makefile.
  '''
  def generateMakefile (self):
    code = ""
    
    code += self.params ()

    code += "\n"
    
    code += self.targetAll ()
    
    code += "\n"
    
    code += self.targetClean ()
    
    return code
  
