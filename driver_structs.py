# -*- coding: utf-8 -*- 
'''
 * Copyright (C) 2016 Jaroslav Kopáček
 *
 * This file is part of Linux Driver Generator.
 *
 * Linux Driver Generator is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Linux Driver Generator is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Linux Driver Generator. If not, see <http://www.gnu.org/licenses/>.
'''
from variable import *

'''
  Class generating device instance structure.
  @class DriverItems
  @brief Class generate file_operations struct and assigned function. This \
  functions can contain user code specified in FEATURE block in definition \
  file.
'''
class DriverItems:
  '''
   @var instanceCounter instance counter
   @brief Static variable storing count of instance in driver (only definitions).
  '''
  instanceCounter = 0

  '''
    Constructor
    @fn __init__
    @param instance True if generate instance variable; false if struct def.
    @brief Class constructor which sets default values.
  '''
  def __init__ (self, instance = False):
    self.isInstance = instance
    self.identifier = ""
    
    if self.isInstance:
      DriverItems.instanceCounter += 1
      self.instanceNumber = DriverItems.instanceCounter
      self.type = "struct driver_items *"
      self.identifier = "instance" + str (self.instanceNumber)
      
    else:
      self.type = "struct driver_items"
    
    self.items = list ()
    self.__setItems ()
    
  '''
    Set default items
    @fn __setItems
    @brief Method sets default items in struct.
  '''
  def __setItems (self):
    self.items.append (Variable("struct device", "my_device", True))
    self.items.append (Variable("dev_t", "dev_num", False))
    self.items.append (Variable("struct cdev", "cdev", True))
    self.items.append (Variable("struct proc_dir_entry *", "proc_entry", True))
    self.items.append (Variable("struct class *", "class", True))
    self.items.append (Variable("struct resource *", "mem", True))
    self.items.append (Variable("struct platform_device *", "pdev", True))
    self.items.append (Variable("u32", "addrsize", False))
    self.items.append (Variable("u32", "physaddr", False))
    self.items.append (Variable("void *", "dev_virtaddr", False))
    
  '''
    Generate structure definition
    @fn generateStructure
    @brief Method generates driver_items structure definitons. If instance \
    flag set, generate instance variable code.
  '''
  def generateStructure (self):
    code = ""

    if self.isInstance:
      code += self.type +  self.identifier + ";\n"

    else:
      code += self.type + " {\n"

      for item in self.items:
        if item.isStruct:
          code += "\t" + item.type + " " + item.identifier + ";\n" 
      
        else:
          code += "\t" + item.type + " " + item.identifier + ";\n" 
          
      code += "};\n"
  
    return code



'''
  Class generating device file operations.
  @class FileOperations
  @brief Class generate file_operations struct and assigned function. This \
  functions can contain user code specified in FEATURE block in definition \
  file.
  Actually supported actions are open, close, read and write.
'''
class FileOperations:  
  '''
    Constructor
    @fn __init__
    @param identifier structure identifier
    @warning Existence of other instances of this class with same identifier \
    in script structures/final code IS NOT CHECKED!
    @brief Class constructor which sets default values.
  '''
  def __init__ (self, identifier = "dev_fop"):
    # TODO ADD CHECK IF IDENTIFIER IS NOT IN USE?!
    
    self.type = "struct file_operations"
    self.identifier = identifier
    self.functionIdentifier = dict ()
    self.__setFunctionIdentifier ()
    self.functionArgs = dict ()
    self.__setFunctionArgs ()
    
  '''
    Default function identifiers
    @fn __setFunctionIdentifier
    @brief Method generate functions identifiers for file operations. This \
    method is called from constructor.
  '''
  def __setFunctionIdentifier (self):
    self.functionIdentifier ["open"] = self.identifier + "_open"
    self.functionIdentifier ["release"] = self.identifier + "_release"
    self.functionIdentifier ["read"] = self.identifier + "_read"
    self.functionIdentifier ["write"] = self.identifier + "_write"
    
  '''
    Default function arguments
    @fn __setFunctionArgs
    @brief Method generate functions arg list for file operations. This \
    method is called from constructor.
  '''    
  def __setFunctionArgs (self):
    self.functionArgs ["open"] = "(struct inode *inode, struct file *filp)"
    self.functionArgs ["release"] = "(struct inode *inode, struct file *filp)"
    self.functionArgs ["read"] = "(struct file *filp, char __user *buf, size_t count, loff_t *f_pos)"
    self.functionArgs ["write"] = "(struct file *filp, const char __user *buf, size_t count, loff_t *f_pos)"
    
    
  '''
    Generate structure definition.
    @fn generateStructure
    @brief Method generates file_operations structure definition. 
  '''
  def generateStructure (self):
    code = ""
    
    code += "static " + self.type + " " + self.identifier + " = {\n"
    
    code += "\t.owner = THIS_MODULE,\n"
    
    for key, value in self.functionIdentifier.items():
      code += "\t." + key + " = " + value + ",\n"
    
    code += "};\n"
    
    return code
    
  '''
    Generate structure's functions.
    @fn generateFunctions
    @brief Method generates file_operations functions definitions.
  '''  
  def generateFunctions (self, moduleTopLevel):
    for key, identifier in self.functionIdentifier.iteritems ():
      userCode = False
      code = ""
      code += "int " + identifier + self.functionArgs[key] + "{\n"
      
      # Try found user code for actual function
      for item in moduleTopLevel.moduleFeatures:
        if item["exec"] == key:
          for var in item["variables"]:
            code += var.generateDefinition () + "\n"
          code +=  "\n"
          code += item["code"] + "\n"
          userCode = True
      
      # Blank body if no user code specified
      if not userCode:
        code += "\treturn 0;\n"
      
      code += "}\n"
      
      # Store generated code in dictionary
      moduleTopLevel.functions[identifier] = code

      userCode = False

'''
  Class generating device file operations.
  @class FileOperations
  @brief Class generate file_operations struct and assigned function. This \
  functions can contain user code specified in FEATURE block in definition \
  file.
  Actually supported actions are open, close, read and write.
'''
class ProcFileOperations:  
  '''
    Constructor
    @fn __init__
    @param identifier structure identifier
    @warning Existence of other instances of this class with same identifier \
    in script structures/final code IS NOT CHECKED!
    @brief Class constructor which sets default values.
  '''
  def __init__ (self, identifier = "proc_fop"):
    # TODO ADD CHECK IF IDENTIFIER IS NOT IN USE?!
    
    self.type = "struct file_operations"
    self.identifier = identifier
    self.functionIdentifier = dict ()
    self.__setFunctionIdentifier ()
    self.functionArgs = dict ()
    self.__setFunctionArgs ()
    
  '''
    Default function identifiers
    @fn __setFunctionIdentifier
    @brief Method generate functions identifiers for file operations. This \
    method is called from constructor.
  '''
  def __setFunctionIdentifier (self):
    self.functionIdentifier ["open"] = self.identifier + "_open"
    self.functionIdentifier ["release"] = self.identifier + "_release"
    self.functionIdentifier ["read"] = self.identifier + "_read"
    self.functionIdentifier ["write"] = self.identifier + "_write"
    
  '''
    Default function arguments
    @fn __setFunctionArgs
    @brief Method generate functions arg list for file operations. This \
    method is called from constructor.
  '''    
  def __setFunctionArgs (self):
    self.functionArgs ["open"] = "(struct inode *inode, struct file *filp)"
    self.functionArgs ["release"] = "(struct inode *inode, struct file *filp)"
    self.functionArgs ["read"] = "(struct file *filp, char __user *buf, size_t count, loff_t *f_pos)"
    self.functionArgs ["write"] = "(struct file *filp, const char __user *buf, size_t count, loff_t *f_pos)"
    
    
  '''
    Generate structure definition.
    @fn generateStructure
    @brief Method generates file_operations structure definition. 
  '''
  def generateStructure (self):
    code = ""
    
    code += "static " + self.type + " " + self.identifier + " = {\n"
    
    code += "\t.owner = THIS_MODULE,\n"
    
    for key, value in self.functionIdentifier.items():
      code += "\t." + key + " = " + value + ",\n"
    
    code += "};\n"
    
    return code
    
  '''
    Generate structure's functions.
    @fn generateFunctions
    @brief Method generates file_operations functions definitions.
  '''  
  def generateFunctions (self, moduleTopLevel):
    for key, identifier in self.functionIdentifier.iteritems ():
      userCode = False
      code = ""
      code += "int " + identifier + self.functionArgs[key] + "{\n"
      
      # Try found user code for actual function
      for item in moduleTopLevel.moduleFeatures:
        if item["exec"] == "proc_" + key:
          for var in item["variables"]:
            code += var.generateDefinition () + "\n"
          code +=  "\n"
          code += item["code"] + "\n"
          userCode = True
      
      # Blank body if no user code specified
      if not userCode:
        code += "\treturn 0;\n"
      
      code += "}\n"
      
      # Store generated code in dictionary
      moduleTopLevel.functions[identifier] = code

      userCode = False
      
      
'''
  Class generating device match structure.
  @class OfDeviceId
  @brief Class generate of_device_id struct. 
'''
class OfDeviceId:
  '''
    Constructor
    @fn __init__
    @param identifier structure identifier
    @warning Existence of other instances of this class with same identifier \
    in script structures/final code IS NOT CHECKED!
    @brief Class constructor which sets default values.
  '''
  def __init__ (self, compatible = "xlnx,xps-gpio-1.00.a", identifier = "dev_match"):
    # TODO ADD CHECK IF IDENTIFIER IS NOT IN USE?!
    self.type = "struct of_device_id"
    self.identifier = identifier
    self.compatible = set ()
    self.addCompatible (compatible)
    
  '''
    Add match string
    @fn addCompatible
    @brief Method adds compatible string into set.
  '''
  def addCompatible (self, compatible = "xlnx,xps-gpio-1.00.a"):
    self.compatible.add (compatible)

  '''
    Generate structure definition
    @fn generateStructure
    @brief Method generates of_device_id structure definitons.
  '''
  def generateStructure (self):
    code = ""
    
    code += "static " + self.type + " " + self.identifier + " [] = {\n"
    
    for item in self.compatible:
      code += "\t{.compatible = \"" + item +"\"},\n"
    
    code += "\t{}\n"
    
    code += "};\n"

    return code


'''
  Class generating platform driver structure.
  @class OfDeviceId
  @brief Class generate platform_driver struct. 
'''
class PlatformDriver:
  '''
    Constructor
    @fn __init__
    @param identifier structure identifier
    @warning Existence of other instances of this class with same identifier \
    in script structures/final code IS NOT CHECKED!
    @brief Class constructor which sets default values.
  '''
  def __init__ (self, identifier = "dev_driver"):
    # TODO ADD CHECK IF IDENTIFIER IS NOT IN USE?!
    self.type = "struct platform_driver"
    self.identifier = identifier
    self.operations = frozenset (["probe", "remove", "shutdown", "suspend", "suspend_late", "resume_early", "resume"])
    self.functionIdentifier = dict ()
    self.addFunctionIdentifier ("probe", "driver_probe")
    self.addFunctionIdentifier ("remove", "driver_remove")    
    
  #def getProbeFunction (self, userVariables = list (), userCode = ""):
    #return ProbeFunction (self, userVariables, userCode)
  
  #def getRemoveFunction (self, userVariables = list (), userCode = ""):
    #return RemoveFunction (self, userVariables, userCode)
  
  
  '''
    Add callback
    @fn addFunctionIdentifier
    @brief Method adds callback to platform_driver struct.
  '''
  def addFunctionIdentifier (self, key, value):
    if key in self.operations:
      self.functionIdentifier[key] = value
    else:
      Exception ("Cannot add unknown driver operation in platform_driver struct.")

  '''
    Generate structure definition
    @fn generateStructure
    @brief Method generates platform_driver structure definitons.
  '''
  def generateStructure (self):
    code = ""
    
    code += "static " + self.type + " " + self.identifier + " = {\n"

    code += "\t.driver = {\n"
    code += "\t\t.name = DRIVER_NAME,\n"
    code += "\t\t.owner = THIS_MODULE,\n"
    code += "\t\t.of_match_table = dev_match,\n"
    code += "\t},\n"

    for key, value in self.functionIdentifier.items ():
      code += "\t." + key + " = " + value +",\n"
    
    code += "};\n"

    return code












