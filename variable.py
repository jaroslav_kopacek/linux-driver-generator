# -*- coding: utf-8 -*- 
'''
 * Copyright (C) 2016 Jaroslav Kopáček
 *
 * This file is part of Linux Driver Generator.
 *
 * Linux Driver Generator is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Linux Driver Generator is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Linux Driver Generator. If not, see <http://www.gnu.org/licenses/>.
'''
import re

'''
  Class reprezenting variable.
  @class RemoveFunction
  @brief Class reprezenting variable.
'''
class Variable:
  '''
    Constructor
    @fn __init__
    @param type type
    @param identifier identifier
    @param isStruct if struct set true
    @param items list of variables for struct
    @param defaultValue for variable
    @param isMember set true if variable is member of struct
    @brief Class constructor which sets default values.
  '''
  def __init__ (self, type = "", identifier = "", isStruct = True, items = list(), defaultValue = "", isMember = False):
    self.type = type
    self.identifier = identifier 
    self.isStruct = isStruct
    self.isMember = isMember
    
    if self.isStruct:
      self.items = items
      self.defaultValue = None
      
    else:
      self.items = None
      self.defaultValue = defaultValue
      
  '''
    Add item to struct.
    @fn addItem
    @brief Method adds items to struct.
  '''
  def addItem (self, item):
    if self.isStruct:
      for it in item:
        it.addIsMember ()
        self.items.append (it)
      
    else:
      raise Exception ("Cannot add item. Variable is not type of stuct!")
    
  '''
    Generate variable's definition.
    @fn generateDefinition
    @brief Method generates variable's definition. 
  '''
  def generateDefinition (self):
    definition = self.type + ' ' + self.identifier
    
    if self.isStruct:
      definition += "{\n"
      for item in self.items:
        ret = item.generateDefinition ()
        if re.match ("(.*;$)", ret, re.UNICODE):
          definition += "\t" + ret + '\n'
        else:
          definition += "\t" + ret + ",\n"
      definition = definition [:-2]
      if not self.isMember:
        definition += "\n};\n"
      else:
        definition += "}\n"
    else:
      if self.defaultValue:
        definition += " = \"" + self.defaultValue + "\""
        
      if not self.isMember:
        definition += ";"
    
    return definition

  '''
    Sets type of variable.
    @fn addType
    @brief Method sets type of variable.
  '''
  def addType (self, type):
    self.type = type
    
  '''
    Sets identifier of variable.
    @fn addIdentifier
    @brief Method sets identifier of variable.
  '''
  def addIdentifier (self, identifier):
    self.identifier = identifier
    
  '''
    Sets isMember flag of variable.
    @fn addIsMember
    @brief Method sets isMember of variable.
  '''
  def addIsMember (self):
    self.isMember = True
    
  '''
    Sets default value of variable.
    @fn addDefaultValue
    @brief Method sets default value of variable.
  '''
  def addDefaultValue (self, defaultValue):
    if not self.isStruct:
      self.defaultValue  = defaultValue

    else:
      raise Exception ("Cannot assign default value to struct variable.")
