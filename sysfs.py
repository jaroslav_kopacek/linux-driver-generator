# -*- coding: utf-8 -*- 
'''
 * Copyright (C) 2016 Jaroslav Kopáček
 *
 * This file is part of Linux Driver Generator.
 *
 * Linux Driver Generator is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Linux Driver Generator is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Linux Driver Generator. If not, see <http://www.gnu.org/licenses/>.
'''

'''
  Class generating support for defined SYSFS attributes
  @class SysFS
  @brief Class generating support for defined SYSFS attributes.
'''
class SysFS:
  '''
    Constructor
    @fn __init__
    @param moduleTopLevel ModuleTopLevel instance
    @param attributes defined attributes from parser
    @brief Class constructor which adds required header files.
  '''
  def __init__ (self, moduleTopLevel, attributes):
    self.moduleTopLevel = moduleTopLevel
    self.attributes = attributes
    
    self.__setHeaders ()
  
  '''
    Method adds required header files.
    @fn __setHeaders
    @brief Method adds required header files.
  '''
  def __setHeaders (self):
    self.moduleTopLevel.addHeaderFile ("linux/sysfs.h")
    self.moduleTopLevel.addHeaderFile ("linux/uaccess.h")

  '''
    Method generates DEVICE_ATTR system macros.
    @fn generateDeviceAttr
    @brief Method generates DEVICE_ATTR system macros
  '''
  def generateDeviceAttr (self):
    code = ""

    for attribute in self.attributes:
      code += "DEVICE_ATTR ("
      
      code += attribute ["name"] + ", " + attribute ["mode"] + ", "
      
      if "show" not in attribute.keys () and "store" not in attribute.keys ():
        raise Exception ("No SYSFS function (show or store) specified.")
            
      if "show" in attribute.keys ():
        code += "sys_" + attribute ["name"] + "_show, "
      
      else:
        code += "NULL, "
      
      if "store" in attribute.keys ():
        code += "sys_" + attribute ["name"] + "_store"
        
      else:
        code += "NULL"
      
      code += ");\n"
    return code

  '''
    Method generates structure attribute as array of attributes.
    @fn generateAttributes
    @brief Method generates structure attribute as array of \
    attributes.
  '''
  def generateAttributes (self):
    code = ""
    
    code+= "static const struct attribute *reg_attrs[] = {\n"
  
    for attribute in self.attributes:
      code += "\t&dev_attr_" + attribute ["name"] + ".attr,\n"

    code += "\tNULL,\n"

    code += "};\n"

    return code

  '''
    Method generates structure attribute_group.
    @fn generateAttributesGroup
    @brief Method generates structure attribute as array of \
    attributes.
    NOTE Actually not used.
  '''
  def generateAttributesGroup (self):
    code = ""
    
    code += "static const struct attribute_group dev_attr_group = {\n"

    code += "\t.attrs = reg_attrs,\n"
    
    code += "};\n"
    
    return code
  
  '''
    Method generates structure attribute_group as array of \
    attribute_groups.
    @fn generateGroups
    @brief Method generates structure attribute_group as array \
    of attribute_groups.
    NOTE Actually not used.
  '''
  def generateGroups (self):
    code = ""
    
    code += "static const struct attribute_group *dev_attr_groups[] = {\n"

    code += "\t&dev_attr_groups,\n"
  
    code += "\tNULL\n"

    code += "};\n"
    
    return code
  
  '''
    Method generates code with all structures.
    @fn generateStructures
    @brief Method generates code with all structures.
  '''
  def generateStructures (self):
    code = ""
    
    code += self.generateDeviceAttr ()
    
    code += "\n"
    
    code += self.generateAttributes ()
    
    code += "\n"
    
    #code += self.generateAttributesGroup ()
    
    #code += "\n"
    
    #code += self.generateGroups ()
    
    #code += "\n"
    
    return code
    
  '''
    Method generates code with all functions.
    @fn generateFunctions
    @brief Method generates code with all functions.
  '''
  def generateFunctions (self):
    code = ""
    
    for attribute in self.attributes:
      if "show" in attribute.keys ():
        code += "static ssize_t sys_" + attribute ["name"] + "_show (struct device *dev, struct device_attribute *attr, char *buf) {\n"
        
        code += attribute ["show"]
        
        code += "}\n\n"
        
      if "store" in attribute.keys ():
        code += "static ssize_t sys_" + attribute ["name"] + "_store (struct device *dev, struct device_attribute *attr, const char *buf, size_t count) {\n"
        
        code += attribute ["store"]
        
        code += "}\n\n"
        
    return code
        
    
    
     
    
    
    
    
    
    
