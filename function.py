# -*- coding: utf-8 -*- 
'''
 * Copyright (C) 2016 Jaroslav Kopáček
 *
 * This file is part of Linux Driver Generator.
 *
 * Linux Driver Generator is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Linux Driver Generator is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Linux Driver Generator. If not, see <http://www.gnu.org/licenses/>.
'''

import pprint
import re

'''
  Class for functions.
  @class RemoveFunction
  @brief Class for functions.
'''
class Function:
  '''
    Constructor
    @fn __init__
    @param returnType return type
    @param identifier identifier
    @param arguments list of arguments
    @param variables variables
    @param body code
    @brief Class constructor which sets default values.
  '''
  def __init__ (self, returnType = "", identifier = "", arguments = list (), variables = list (), body = ""):
    self.arguments = arguments
    self.variables = variables
    self.identifier = identifier
    self.returnType = returnType
    self.body = body
    
  '''
    Generate function's header.
    @fn generateFunctionHeader
    @brief Method generates function's header. 
  '''
  def generateFunctionHeader (self):
    functionHeader = (self.returnType + ' ' + self.identifier)
    functionHeader += '('
    if self.arguments:
      for arg in self.arguments:
        functionHeader += arg.type + ' ' + arg.identifier + ", "
      functionHeader = functionHeader [:-2]
    functionHeader += ")"
    
    return functionHeader
    
  '''
    Generate function's body.
    @fn generateFunctionBody
    @brief Method generates function's body. 
  '''
  def generateFunctionBody (self):
    code = ""
    for var in self.variables:
      code += var.generateDefinition ()
      
    code += '\n'
    code += self.body
    return code
  
  '''
    Generates entire function.
    @fn generateEntireFunction
    @brief Method generates entire function. 
  '''
  def genereteEntireFunction (self):
    code = ""
    code += self.generateFunctionHeader ()
    code += "{\n"
    code += self.generateFunctionBody ()
    code += "}\n"
    
    return code
  
  '''
    Sets function arguments
    @fn addArguments
    @brief Method sets function arguments.
  '''
  def addArguments (self, arguments):
    if type (arguments) is Variable:
      self.arguments.append (arguments)
      
    elif type (arguments) is list:
      for arg in arguments:
        self.arguments.append (arg)
    
    else:
      raise Exception ("Cannot add argument to function. Argoment is not type Variable nor list.")
    
  '''
    Sets function variables
    @fn addVariables
    @brief Method sets function variables.
  '''
  def addVariables (self, variables):
    if type (variables) is Variable:
      self.variables.append (variables)
      
    elif type (variables) is list:
      for arg in variables:
        self.variables.append (arg)
    
    else:
      raise Exception ("Cannot add variable to function. Argoment is not type Variable nor list.")
    
  '''
    Sets function body
    @fn addBody
    @brief Method sets function body.
  '''
  def addBody (self, body):
    if type (body) is str:
      self.body += body
    else:
      raise Exception ("Cannot add variable to function. Argoment is not type Variable nor list.")
    
    
  '''
    Seeking function variables by identifier.
    @fn getVariableByIdentifier
    @brief Seeking function variables by identifier.
  '''
  def getVariableByIdentifier (self, identifier, variables = None):
    if not variables:
      variables = self.variables

    for var in variables:
      if var.identifier == identifier:
        return var.identifier
      
      if var.isStruct:
        ret = self.getVariableByIdentifier (type, var.items)
        if ret:
          if re.match ('(\*)', var.type):
            return var.identifier + "->" + ret
          else:
            return var.identifier + "." + ret
    
  '''
    Seeking function variables by type.
    @fn getVariableByType
    @brief Seeking function variables by type.
  '''
  def getVariableByType (self, type, variables = None):
    if not variables:
      variables = self.variables

    for var in variables:
      if var.type == type:
        return var.identifier
      
      if var.isStruct:
        ret = self.getVariableByType (type, var.items)
        if ret:
          if re.match ('(\*)', var.type):
            return var.identifier + "->" + ret
          else:
            return var.identifier + "." + ret
    
  '''
    Seeking function arguments by identifier.
    @fn getArgumentByIdentifier
    @brief Seeking function arguments by identifier.
  '''
  def getArgumentByIdentifier (self, identifier, arguments = None):
    if not arguments:
      arguments = self.arguments

    for arg in arguments:
      if arg.identifier == identifier:
        return arg.identifier
      
      if arg.isStruct:
        ret = self.getVariableByIdentifier (type, arg.items)
        if ret:
          if re.match ('(\*)', arg.type):
            return arg.identifier + "->" + ret
          else:
            return arg.identifier + "." + ret
    
  '''
    Seeking function arguments by type.
    @fn getArgumentByType
    @brief Seeking function arguments by type.
  '''
  def getArgumentByType (self, type, arguments = None):
    if not arguments:
      arguments = self.arguments

    for arg in arguments:
      if arg.type == type:
        return arg.identifier
      
      if arg.isStruct:
        ret = self.getVariableByType (type, arg.items)
        if ret:
          if re.match ('(\*)', arg.type):
            return arg.identifier + "->" + ret
          else:
            return arg.identifier + "." + ret
          
          
  '''
    Generating comments.
    @fn createComment
    @brief Generating comments.
  '''
  def createComment (self, description, multiline = False):
    comment = ""
    chunks, chunkSize = len(description), 80
    if multiline and chunks > chunkSize:
      comment += "/*\n"
      for i in range(0, chunks, chunkSize):
        comment += " * " + description [i:i+chunkSize] + "\n"
      comment += " */\n"
      
    elif multiline and chunks < chunkSize:
      comment += "/*\n"
      comment += " * " + description + "\n"
      comment += " */\n"
      
    else:
      comment += "// " + description
      
    return comment + '\n'

    # NOTE Firstly process module def subblocks
    
    
  '''
    Generating conditions.
    @fn createCondition
    @brief Generating conditions.
    TODO This method could be rewrite to generating complete if-then-else construction
    include statements. Maybe later.
  '''
  def createCondition (self, op1, op2, operator, assignments = False, op3 = None):
    if assignments and op3:
      return "if ((" + op1 + " = " + op2 + ") " + operator + " "  + op3 + ")"
  
    elif not assignments and not op3:
      return "if (" + op1 + " " + operator + " "  + op2 + ")"
    
    else:
      raise Exception ("(create_condition) Bad params combination.")


    
