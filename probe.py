# -*- coding: utf-8 -*- 
'''
 * Copyright (C) 2016 Jaroslav Kopáček
 *
 * This file is part of Linux Driver Generator.
 *
 * Linux Driver Generator is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Linux Driver Generator is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Linux Driver Generator. If not, see <http://www.gnu.org/licenses/>.
'''
from driver_structs import *
from variable import *
from function import *

'''
  Class generating probe function.
  @class ProbeFunction
  @brief Class generates probe function.
'''
class ProbeFunction (Function):
  '''
    Constructor
    @fn __init__
    @param moduleTopLevel ModuleTopLevel instance
    @param userVariables variables from FEATURE block where exec is equal to init
    @param userCode code from FEATURE block where exec is equal to init
    @brief Class constructor which sets default values.
  '''
  def __init__ (self, moduleTopLevel, userVariables = list (), userCode = ""):
    self.moduleTopLevel = moduleTopLevel
    self.arguments = list ()
    self.setArguments ()
    self.variables = userVariables
    self.setVariables ()
    self.identifier = ""
    self.setIdentifier ()
    self.returnType = ""
    self.setReturnType ()
    self.userCode = userCode
    
  '''
    Default function arguments
    @fn setArguments
    @brief Method sets default arguments.
  '''
  def setArguments (self):
    self.arguments.append (Variable ("struct platform_device *", "pdev", False))
    
  '''
    Default function variables
    @fn setVariables
    @brief Method sets default variables.
  '''
  def setVariables (self):
    #self.variables.append (Variable ("int", "state", False))
    self.variables.append (Variable ("struct resource *", "dev_resource", False))
    
  '''
    Sets function return type
    @fn setReturnType
    @brief Method sets return type.
  '''
  def setReturnType (self):
    self.returnType = "int"
    
  '''
    Sets function identifier
    @fn setIdentifier
    @brief Method sets identifier.
  '''
  def setIdentifier (self):
    self.identifier = "driver_probe"
    
  '''
    Generate code for getting resources.
    @fn getResource
    @brief Method generates code for getting resources. 
  '''
  def getResource (self):
    code = ""
    
    code += Function.createComment (self, "Get resource")
    
    code += self.getVariableByType ("struct resource *") + " = platform_get_resource (" + self.getArgumentByType ("struct platform_device *") + ", IORESOURCE_MEM, 0);"
    
    code += self.createCondition (self.getVariableByType ("struct resource *"), "NULL", "==") + "{\n"
    
    code += "\t dev_err (&" + self.getArgumentByType ("struct platform_device *") + "->dev, \"No resource found\");\n"
    
    code += "\t goto fail;\n"
    
    code += "}\n"
    
    return code
  
  '''
    Generate code for allocating private structure.
    @fn allocPrivateStruct
    @brief Method generates code for allocating private structure. 
  '''
  def allocPrivateStruct (self):
    code = ""
    
    code += Function.createComment (self, "Alloc private structure for managing this device")
    
    code += self.moduleTopLevel.globalVariables["driver_instance"].identifier + " = kmalloc (sizeof (" + self.moduleTopLevel.globalVariables["driver_items"].type + "), GFP_KERNEL);\n"
    
    code += self.createCondition (self.moduleTopLevel.globalVariables["driver_instance"].identifier, "NULL", "==") + "{\n"
    
    code += "\t dev_err (&" + self.getArgumentByType ("struct platform_device *") + "->dev, \"Unable allocate device structure\");\n"
    
    code += "\t goto fail;\n"
    
    code += "}\n"
    
    return code
  
  '''
    Generate code for allocation region.
    @fn getPlatformDevice
    @brief Method generates code for allocating region. 
  '''
  def getPlatformDevice (self):
    return Function.createComment (self, "Get resource") + self.moduleTopLevel.globalVariables["driver_instance"].identifier + "->pdev = " + self.getArgumentByType ("struct platform_device *") + ';\n'
  
  '''
    Generate code for getting device number.
    @fn getDeviceNumber
    @brief Method generates code for getting device number. 
  '''
  def getDeviceNumber (self):
    code = ""
    
    code += Function.createComment (self, "Device number (major, minor)")
    
    code += Function.createCondition (self, "alloc_chrdev_region (&" + self.moduleTopLevel.globalVariables["driver_instance"].identifier + "->dev_num, 0, 1, " + self.moduleTopLevel.define["driverName"]["name"] +")", "0", "<") + "{\n"
    
    code += "\t dev_err (&" + self.getArgumentByType ("struct platform_device *") + "->dev, \"Unable register chrdev\");\n"
    
    code += "\t goto fail;\n"
    
    code += "}\n"
    
    #code += "PDEBUG (KERN_DEBUG \"Given device number: MAJOR: %d MINIOR: %d\\n \", MAJOR (" + self.moduleTopLevel.globalVariables["driver_instance"].identifier + "->dev_num), MINOR (" + self.moduleTopLevel.globalVariables["driver_instance"].identifier + "->dev_num));\n"
    
    return code
  
  '''
    Generate code for getting SYSFS entry.
    @fn sysfsEntry
    @brief Method generates code for getting SYSFS entry. 
  '''
  def sysfsEntry (self):
    code = ""
    
    code += Function.createCondition (self, "sysfs_create_files (&pdev->dev.kobj, reg_attrs)", "0", "!=") + "{\n"
    
    code += "\t dev_err (&" + self.getArgumentByType ("struct platform_device *") + "->dev, \"Cannot create proc entry\");\n"
    
    code += "\t goto fail;\n"
        
    code += "}\n"

    return code
  
  '''
    Generate code for getting PROCFS entry.
    @fn procEntry
    @brief Method generates code for getting PROCFS entry. 
  '''
  def procEntry (self):
    code = ""
    
    code +=  self.moduleTopLevel.globalVariables["driver_instance"].identifier + "->proc_entry = proc_create (\"driver/\"" + self.moduleTopLevel.define["driverName"]["name"] + ", 0644, NULL, &" + self.moduleTopLevel.globalVariables["proc_fops"].identifier + ");\n"
    
    code += Function.createCondition (self, "(" + self.moduleTopLevel.globalVariables["driver_instance"].identifier + "->proc_entry)", "NULL", "==") + "{\n"
    
    code += "\t dev_err (&" + self.getArgumentByType ("struct platform_device *") + "->dev, \"Cannot create proc entry\");\n"
    
    code += "\t goto fail;\n"
    
    code += "}\n"
    
    return code
  
  '''
    Generate code for device registrations.
    @fn deviceRegister
    @brief Method generates code for device registrations. 
  '''
  def deviceRegister (self):
    # TODO ADD FOPS STRUCTURE TO TOP LEVEL
    
    code = ""
    
    code += Function.createComment (self, "Register with the kernel as a chardev device")
    
    code += "cdev_init(&" + self.moduleTopLevel.globalVariables["driver_instance"].identifier + "->cdev, &" + self.moduleTopLevel.globalVariables["dev_fops"].identifier + ");\n"
  
    code += self.moduleTopLevel.globalVariables["driver_instance"].identifier + "->cdev.owner = THIS_MODULE;\n"
  
    code += self.moduleTopLevel.globalVariables["driver_instance"].identifier + "->cdev.ops = &" +  self.moduleTopLevel.globalVariables["dev_fops"].identifier + ";\n"
  
    return code
  
  '''
    Generate code for getting memory access.
    @fn getMemoryAccess
    @brief Method generates code for getting memory access. 
  '''
  def getMemoryAccess (self):
    code = ""
    
    code += Function.createComment (self, "Get memory access")
    
    code += self.moduleTopLevel.globalVariables["driver_instance"].identifier + "->physaddr = " + self.getVariableByType ("struct resource *") + "->start;\n"

    code += self.moduleTopLevel.globalVariables["driver_instance"].identifier + "->addrsize = " + self.getVariableByType ("struct resource *") + "->end - "+ self.getVariableByType ("struct resource *") + "->start + 1;\n"

    code += Function.createCondition (self, "request_mem_region (" + self.moduleTopLevel.globalVariables["driver_instance"].identifier + "->physaddr, "+ self.moduleTopLevel.globalVariables["driver_instance"].identifier + "->addrsize, MODULE_NAME)", "0", "==") + "{\n"
    
    code += "\t dev_err (&" + self.getArgumentByType ("struct platform_device *") + "->dev, \"Unable register chrdev\");\n"
    
    code += "\t goto fail;\n"
    
    code += "}\n"
    
    code += self.moduleTopLevel.globalVariables["driver_instance"].identifier + "->dev_virtaddr = of_iomap(" + self.moduleTopLevel.globalVariables["driver_instance"].identifier + "->pdev->dev.of_node, 0);\n"
  
    return code
  
  '''
    Generate code for adding cdev to kernel.
    @fn addCdev
    @brief Method generates code for adding cdev to kernel. 
  '''
  def addCdev (self):
    code = ""
    
    code += Function.createComment (self, "Add device to kernel")
    
    code += Function.createCondition (self, "cdev_add (&" + self.moduleTopLevel.globalVariables["driver_instance"].identifier + "->cdev, " + self.moduleTopLevel.globalVariables["driver_instance"].identifier + "->dev_num, 1)", "0", "<") + "{\n"
    
    code += "\t dev_err (&" + self.getArgumentByType ("struct platform_device *") + "->dev, \"Unable add chrdev\");\n"
    
    code += "\t goto fail;\n"
    
    code += "}\n"
    
    return code
  
  '''
    Generate fail code.
    @fn fail
    @brief Method generates fail code. 
  '''
  def fail (self):
    code = ""
    
    code += "fail:\n"
    code += "\tdriver_remove (pdev);\n" ### TODO After adding support for seeking function in MTL replace function's identifier with calling this seek function.
    code += "\treturn -ENODEV;\n"
    
    return code

  '''
    Generate function's body.
    @fn generateFunctionBody
    @brief Method generates function's body. 
  '''
  def generateFunctionBody (self):
    code = ""
    
    # VARIABLES DEFINITIONS
    for var in self.variables:
      code += var.generateDefinition () + "\n"
      
    code += '\n'
    
    # PROBE CODE
    code += self.getResource ()
    code += "\n"
    code += self.allocPrivateStruct ()
    code += "\n"
    code += self.getPlatformDevice ()
    code += "\n"
    code += self.getDeviceNumber ()
    code += "\n"
    
    # PLACE FOR PROCFS AND SYSFS CODE
    if self.moduleTopLevel.procfsFlag:
      code += self.procEntry ()

    if self.moduleTopLevel.sysfsFlag:
      code += self.sysfsEntry ()
    
    code += self.deviceRegister ()
    code += "\n"
    code += self.getMemoryAccess ()
    code += "\n"
    code += self.addCdev ()
    code += "\n"
  
    # PLACE FOR USER CODE 
    code += Function.createComment (self, "USER CODE")
    code += self.userCode
    code += '\n'

    code += self.fail ()
    code += "\n"
    
    self.body = code
    
    return code
    
  '''
    Generates entire function.
    @fn generateEntireFunction
    @brief Method generates entire function. 
  '''
  def generateEntireFunction (self):
    code = ""
    code += Function.generateFunctionHeader (self)
    code += "{\n"
    code += self.generateFunctionBody ()
    code += "}\n"
    
    return code
  
  
  
  
  
  
  
