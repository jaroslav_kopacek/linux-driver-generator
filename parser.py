# -*- coding: utf-8 -*- 
'''
 * Copyright (C) 2016 Jaroslav Kopáček
 *
 * This file is part of Linux Driver Generator.
 *
 * Linux Driver Generator is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Linux Driver Generator is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Linux Driver Generator. If not, see <http://www.gnu.org/licenses/>.
'''

import pprint
import time
from variable import *

'''
  TODO WARNING NOTE: In module_def have to be some params processed only for platform drivers 
  and for non-platform drivers can be processed others. Some params can be used in both ways.
'''

'''
  Class parsing input definition file.
  @class Parser
  @brief Class parsing input definition file.
'''
class Parser:
  MODULE_DESC = "MODULE_DESC"
  MODULE_DEF = "MODULE_DEF"
  
  '''
    Constructor
    @fn __init__
    @param desc_file definition file
    @brief Class constructor which sets default values. Constructor sets \
    names of blocks and params. Constructor also prepares output variables.
  '''
  def __init__ (self, desc_file):
    # FILE PART
    self.file = open (desc_file, 'r')
    self.line = "";
    self.last_fp = 0
    
    # NAMES OF BLOCKS AND PARAMS
    self.desc_params = frozenset (["author", "license", "description", "date"])
    self.def_params = frozenset (["module_name", "driver_name", "driver_type", "compatible", "register_addr", "register_size"])
    self.def_subblocks = frozenset (["DEFINE", "HEADER", "FEATURE", "SYSFS", "FUNCTION"]) # "DEFINE", "HEADER", "FEATURE", 
    self.feature_params = frozenset (["description", "exec", "VARIABLES", "STRUCTURE", "CODE"])
    self.sysfs_params = frozenset (["name", "mode", "VARIABLES", "SHOW", "STORE"])
    self.function_params = frozenset (["description", "VARIABLES", "CODE"])
    
    # OUTPUT VARIABLES
    self.headers = set ()
    self.own_headers = set ()
    self.defines = set ()
    self.module_info = dict ()
    self.module_def = dict ()
    self.module_features = list ()
    self.module_def_subblocks = list ()
    self.functions = list ()
    self.sysfs = list ()
  
  '''
    Basic parser method.
    @fn parse_file
    @brief Basic parser method.  
  '''
  def parse_file (self):
    while True:
      self.line = self.file.readline ()
      if self.line == "":
        break

      
      # Skip blank lines
      if (re.match ('(.*\S.*)', self.line, re.UNICODE) == None):
        continue
      
      # Erase all at the begin and end of string
      self.line = self.line.strip()
      
      #print self.line

      if re.match ("^" + re.escape (self.MODULE_DESC)+":\s*$", self.line, re.UNICODE) != None:
        #print ("module_desc matched")
        self.fpos = self.file.tell ();
        self.parse_module_info ()

      if re.match ("^" + re.escape (self.MODULE_DEF)+":\s*$", self.line, re.UNICODE) != None:
        #print ("module_def matched")
        self.fpos = self.file.tell ();
        self.parse_module_def ()
        
    if __debug__:
      print "----------------------------------\nmodule info:"
      pprint.pprint (self.module_info)
      print "----------------------------------\nmodule def:"
      pprint.pprint (self.module_def)
      print "----------------------------------\nheaders:"
      pprint.pprint (self.headers)
      print "----------------------------------\nown_headers:"
      pprint.pprint (self.own_headers)
      print "----------------------------------\ndefines:"
      pprint.pprint (self.defines)
      print "----------------------------------\nmodule_features:"
      pprint.pprint (self.module_features)
      print "----------------------------------\nfunctions:"
      pprint.pprint (self.functions)
      print "----------------------------------\nsysfs:"
      pprint.pprint (self.sysfs)
        
  '''
    Matching parameters.
    @fn match_param
    @param param param for matching
    @param group group of match
    @brief Matching parameters.
  '''
  def match_param (self, param, group = 1):
    # Match string param
    result = re.match ('(?:\s*' + re.escape (param) + ':\s*"(.*)"\s*)', self.line, re.UNICODE)
    if result != None:
      return result.group (group)
    
    # Match number param
    result = re.match ('(?:\s*' + re.escape (param) + ':\s*(.*)\s*)', self.line, re.UNICODE)
    if result != None:
      return result.group (group)

    return False

  '''
    Matching subblock.
    @fn match_param
    @param param subblock for matching
    @param group group of match
    @brief Matching subblock.
  '''
  def match_subblock (self, param, group = 1):
    result = re.match ("(?:\s*(" + re.escape (param) + ")\s*:\s*)", self.line, re.UNICODE)
    if result != None:
      return result.group (group)

    return False

  '''
    Method parsing MODULE_INFO block.
    @fn parse_module_info
    @brief Method parsing MODULE_INFO block.
  '''
  def parse_module_info (self):
    self.file.seek (self.fpos)
    
    result = ""
    
    while True:
      self.line = self.file.readline()
      
      # End of description block
      if re.match ("^\s*" + self.MODULE_DEF + ":\s*", self.line, re.UNICODE) != None or self.line == '':
        #raise Exception ("(parse_module_info) Unexpected EOF")
        break
      
      for param in self.desc_params:
        result = self.match_param (param)
        if result:
          if param == "author":
            self.module_info['author'] = result
            #print "Result stored in module_info! (author) Result is: " + self.module_info['author']
          
          elif param == "license":
            self.module_info['license'] = result
            #print "Result stored in module_info! (license) Result is: " + self.module_info['license']
          
          elif param == "description":
            self.module_info['description'] = result
            #print "Result stored in module_info! (description) Result is: " + self.module_info['description']
          
          elif param == "date":
            self.module_info['date'] = result
            #print "Result stored in module_info! (date) Result is: " + self.module_info['date']
        
    if "author" not in self.module_info or "license" not in self.module_info:
      raise Exception ("(parse_module_info) Missing author or license.")

  '''
    Method parsing MODULE_DEF block.
    @fn parse_module_def
    @brief Method parsing MODULE_DEF block.
  '''
  def parse_module_def (self):
    #self.file.seek (self.fpos)

    result = ""
    while True:
      self.line = self.file.readline ()
      #print "(parse_module_def) actual line: " + self.line
      if self.line.count == 0:
        break
      
      # End of description block
      if re.match ("^\s*" + self.MODULE_DESC + ":\s*", self.line, re.UNICODE) != None or self.line == '':
        break
      
      for param in self.def_params:
        result = self.match_param (param)
        if result:
          if param == "driver_name":
            self.module_def['driver_name'] = result
            #print "Result stored in module_def! (dev_name) Result is: " + self.module_def['dev_name']
          
          elif param == "driver_type":
            self.module_def['driver_type'] = result
            #print "Result stored in module_def! (driver_type) Result is: " + self.module_def['driver_type']
          
          elif param == "module_name":
            self.module_def['module_name'] = result
            #print "Result stored in module_def! (driver_type) Result is: " + self.module_def['driver_type']

          elif param == "compatible":
            self.module_def['compatible'] = result.strip ("<\">")
            #print "Result stored in module_def! (driver_type) Result is: " + self.module_def['driver_type']

          elif param == "register_addr":
            self.module_def['register_addr'] = result
            #print "Result stored in module_def! (driver_type) Result is: " + self.module_def['driver_type']
            
          elif param == "register_size":
            self.module_def['register_size'] = result
            #print "Result stored in module_def! (driver_type) Result is: " + self.module_def['driver_type']
            
      for subblock in self.def_subblocks:
        result = re.match ("(?:\s*(" + subblock + ")\s*:\s*)", self.line, re.UNICODE)
        if result:
          result = result.group (1)
          #print result
          if result == "HEADER":
            #print "HEADER subblock matched"
            self.parse_header ()

          elif result == "DEFINE":
            #print "DEFINE subblock matched"
            self.parse_define ()

          elif result == "FEATURE":
            #print "FEATURE subblock matched"
            self.parse_feature ()
            
          elif result == "SYSFS":
            #print "SYSFS subblock matched"
            self.parse_sysfs ()

          elif result == "FUNCTION":
            #print "FUNCTION subblock matched but not processed. Actually not suppported block."
            self.parse_function ()

          '''
            TODO Later will be added PROCFS and SYSFS
          elif subblock == "PROCFS":
            #print "PROCFS subblock matched"
            self.parse_procfs ()
            
          elif subblock == "SYSFS":
            #print "SYSFS subblock matched"
            self.parse_sysfs ()
          '''
            
    if "driver_name" not in self.module_def:# or not self.module_def_subblocks:
      raise Exception ("(parse_module_def) Missing device name [dev_name] or at least one feature not specified.")
    ''' 
    for item in self.module_def_subblocks:
      if item ["type"] == "FEATURE":
        if "CODE" in item and "exec" in item:
          return 
        
        else:
          raise Exception ("Feature block has not specified execution time [exec] or code block [code].")
          
      
    raise Exception ("(parse_module_def) At least one feature not specified.")
    '''
    
  '''
    Method parsing HEADER block.
    @fn parse_header
    @brief Method parsing HEADER block.
  '''
  def parse_header (self):
    result = ""
    while True:
      self.last_fp = self.file.tell ()
      self.line = self.file.readline ()
      if self.line == "":
        break
      
      if (re.match ('(.*\S.*)', self.line, re.UNICODE) == None):
        continue
      
      # Erase all at the begin and end of string
      self.line = self.line.strip()
  
      #print self.line
      
      result = self.match_param ("header")
      if result:
        self.headers.add (result)
        
      else:
        result = self.match_param ("own_header")
        if result:
          self.own_headers.add (result)

        else:
          for param in self.def_params:
            if self.match_param (param):
              self.file.seek (self.last_fp)
              return

          for subblock in self.def_subblocks:
            if self.match_subblock (subblock):
              self.file.seek (self.last_fp)
              return
        
          raise Exception ("Unknown parameter in HEADER block")
          
  '''
    Method parsing DEFINE block.
    @fn parse_define
    @brief Method parsing DEFINE block.
  '''
  def parse_define (self):
    result = ""
    while True:
      self.last_fp = self.file.tell ()
      self.line = self.file.readline ()
      if self.line == "":
        break

      if (re.match ('(.*\S.*)', self.line, re.UNICODE) == None):
        continue
      
      # Erase all at the begin and end of string
      self.line = self.line.strip()
  
      result = self.match_param ("define")

      if result:
        self.defines.add (result)
        
      else:
        for param in self.def_params:
          if self.match_param (param):
            self.file.seek (self.last_fp)
            return

        for subblock in self.def_subblocks:
          if self.match_subblock (subblock):
            self.file.seek (self.last_fp)
            return
        
        raise Exception ("Unknown parameter in DEFINE block")
    
  '''
    Method parsing FEATURE block.
    @fn parse_feature
    @brief Method parsing FEATURE block.
  '''
  def parse_feature (self):
    params = dict ()
    params["variables"] = list()
    result = ""
    
    while True:
      self.line = self.file.readline ()
      if self.line == "":
        break
      
      # End of description block
      if re.match ("^\s*}\s*", self.line, re.UNICODE) != None:
        #print "leaving parse_feature ()"
        break

      '''
      for param in self.def_subblocks:
        #print "param: " + param +" actual line: " + self.line
        if re.match ("(?:\s*(" + param + ")\s*:\s*)", self.line, re.UNICODE):
          self.file.seek (self.last_fp)
          self.module_features.append (params)
          return
          #break
      '''

      #print self.line

      for param in self.feature_params:
        result = self.match_param (param)
        if result:
          if param == "description":
            params['description'] = result
            #print "Result stored in params! (description) Result is: " + params['description']
            
          elif param == "exec":
            params['exec'] = result
            #print "Result stored in params! (exec) Result is: " + params['exec']
            
        result = re.match ("(?:\s*(" + param + ")\s*:\s*)", self.line, re.UNICODE)
        if result:
          if param == "VARIABLES":
            if "variable" in params:
              params ['variables'] = self.process_variables ()
            else:
              for item in self.process_variables ():
                params ['variables'].append (item)
            
          elif param == "STRUCTURE":
            if "variable" in params:
              params ['variables'] = self.process_structs ()
            else:
              params ['variables'].append (self.process_structs ())
            
            #print "Result stored in params! (repeat) Result is: " + params['repeat']
          elif param == "CODE":
            params ['code'] = self.process_code_block ()
       
      for param in self.def_params:
        #print "param: " + param +" actual line: " + self.line
        if re.match ("(?:\s*(" + param + ")\s*:\s*)", self.line, re.UNICODE):
          self.file.seek (self.last_fp)
          if "exec" in params.keys ():
            self.module_features.append (params)
          return

      for param in self.def_subblocks:
        #print "param: " + param +" actual line: " + self.line
        if re.match ("(?:\s*(" + param + ")\s*:\s*)", self.line, re.UNICODE):
          self.file.seek (self.last_fp)
          if "exec" in params.keys ():
            self.module_features.append (params)
          return

    self.file.seek (self.last_fp)
    if "exec" in params.keys ():
      self.module_features.append (params)
              
    ### TODO if 'driver_type' not in params raise EXCEPTION (NOT IMPLEMENTED YET!!!!)
        
  '''
    Method parsing CODE block.
    @fn process_code_block
    @brief Method parsing CODE block.
  '''
  def process_code_block (self):
    code = ""
    while True:
      self.last_fp = self.file.tell ()
      self.line = self.file.readline ()
      
      if self.line == "":
        return code

      if (re.match ('(.*\S.*)', self.line, re.UNICODE) == None):
        continue
      
      #print self.line
            
      # Erase all at the begin and end of string
      self.line = self.line.strip()
      
      for param in self.feature_params:
        #print "param: " + param +" actual line: " + self.line
        if re.match ("(?:\s*(" + param + ")\s*:\s*)", self.line, re.UNICODE):
          self.file.seek (self.last_fp)
          return code
      
      for param in self.def_params:
        #print "param: " + param +" actual line: " + self.line
        if re.match ("(?:\s*(" + param + ")\s*:\s*)", self.line, re.UNICODE):
          self.file.seek (self.last_fp)
          return code

      for param in self.def_subblocks:
        #print "param: " + param +" actual line: " + self.line
        if re.match ("(?:\s*(" + param + ")\s*:\s*)", self.line, re.UNICODE):
          self.file.seek (self.last_fp)
          return code
      
        
        
      code += self.line
      code += '\n'
        
  '''
    Method parsing VARIABLES block.
    @fn process_variables
    @brief Method parsing VARIABLES block.
  '''
  def process_variables (self):
    variables = list ()
    while True:
      self.last_fp = self.file.tell ()
      self.line = self.file.readline ()
      
      if self.line == "":
        break
      
      
      if (re.match ('(.*\S.*)', self.line, re.UNICODE) == None):
        continue
      
      # Erase all at the begin and end of string
      self.line = self.line.strip()

      if self.line == "STRUCTURE_END":
        return variables
      
      #print self.line
      
      for param in self.feature_params:
        #print "param: " + param +" actual line: " + self.line
        if re.match ("(?:\s*(" + param + ")\s*:\s*)", self.line, re.UNICODE):
          self.file.seek (self.last_fp)
          return variables
      
      for param in self.sysfs_params:
        #print "param: " + param +" actual line: " + self.line
        if re.match ("(?:\s*(" + param + ")\s*:\s*)", self.line, re.UNICODE):
          self.file.seek (self.last_fp)
          return variables
      
        
      type, identifier = self.line.split (':') 
      variables.append (Variable (type, identifier, False))
    #print "return outside loop"
    return variables

  '''
    Method parsing STRUCTURE block.
    @fn process_structs
    @brief Method parsing STRUCTURE block.
  '''
  def process_structs (self):
    struct = Variable ()
    while True:
      self.last_fp = self.file.tell ()
      self.line = self.file.readline ()
      
      if self.line == "":
        break
      
      
      if (re.match ('(.*\S.*)', self.line, re.UNICODE) == None):
        continue
      
      # Erase all at the begin and end of string
      self.line = self.line.strip()

      #print self.line
      
      if re.match ("(?:\s*(VARIABLES)\s*:\s*)", self.line, re.UNICODE):
        struct.addItem (self.process_variables())
        
      if self.line == "STRUCTURE_END":
        return struct
      
      if re.match ("(?:\s*(STRUCTURE)\s*:\s*)", self.line, re.UNICODE):
        struct.addItem(self.process_structs ())
        continue
      
      for param in self.feature_params:
        #print "param: " + param +" actual line: " + self.line
        if re.match ("(?:\s*(" + param + ")\s*:\s*)", self.line, re.UNICODE):
          self.file.seek (self.last_fp)
          return struct
      
      for param in self.sysfs_params:
        #print "param: " + param +" actual line: " + self.line
        if re.match ("(?:\s*(" + param + ")\s*:\s*)", self.line, re.UNICODE):
          self.file.seek (self.last_fp)
          return struct
        
      type, identifier = self.line.split (':') 
      struct.addType (type)
      struct.addIdentifier (identifier)
    #print "return outside loop"
    return struct

  '''
    Method parsing FUNCTION block.
    @fn parse_function
    @brief Method parsing FUNCTION block.
  '''
  def parse_function (self):
    code = ""

    while True:
      self.last_fp = self.file.tell ()
      self.line = self.file.readline ()
      if self.line == "":
        break

      if (re.match ('(.*\S.*)', self.line, re.UNICODE) == None):
        continue
      
      # Erase all at the begin and end of string
      self.line = self.line.strip()
  
      for param in self.def_params:
        if self.match_param (param):
          self.file.seek (self.last_fp)
          self.functions.append (code)
          return

      for subblock in self.def_subblocks:
        if self.match_subblock (subblock):
          self.file.seek (self.last_fp)
          self.functions.append (code)
          return
      
      code += self.line + "\n"
      
    self.functions.append (code)

  '''
    Method parsing SYSFS block.
    @fn parse_sysfs
    @brief Method parsing SYSFS block.
  '''
  def parse_sysfs (self):
    params = dict ()
    params["variables"] = list()
    result = ""
    
    while True:
      self.line = self.file.readline ()
      if self.line == "":
        break

      # End of description block
      if re.match ("^\s*}\s*", self.line, re.UNICODE) != None:
        #print "leaving parse_feature ()"
        break
      
      #print self.line

      for param in self.sysfs_params:
        result = self.match_param (param)
        if result:
          if param == "name":
            params['name'] = result
            #print "Name stored in params!"
            
          elif param == "mode":
            params['mode'] = result
            #print "Result stored in params! (mode)"
            
        result = re.match ("(?:\s*(" + param + ")\s*:\s*)", self.line, re.UNICODE)
        if result:
          if param == "VARIABLES":
            if "variable" in params:
              params ['variables'] = self.process_sysfs_variables ()
            else:
              for item in self.process_sysfs_variables ():
                params ['variables'].append (item)
            
          elif param == "STRUCTURE":
            if "variable" in params:
              params ['variables'] = self.process_sysfs_structs ()
            else:
              params ['variables'].append (self.process_sysfs_structs ())
            
            #print "Result stored in params! (repeat) Result is: " + params['repeat']
          elif param == "SHOW":
            params ['show'] = self.process_sysfs_function_code ()

          elif param == "STORE":
            params ['store'] = self.process_sysfs_function_code ()
              
      for param in self.def_params:
        if self.match_param (param):
          self.file.seek (self.last_fp)
          self.sysfs.append (params)
          return

      for subblock in self.def_subblocks:
        if self.match_subblock (subblock):
          self.file.seek (self.last_fp)
          self.sysfs.append (params)
          return

          
    #print "sysfs block:"
    #pprint.pprint (params)
    self.sysfs.append (params)

  '''
    Method processing SHOW and STORE code blocks.
    @fn process_sysfs_function_code
    @brief Method processing SHOW and STORE code blocks
  '''
  def process_sysfs_function_code (self):
    code = ""
    while True:
      self.last_fp = self.file.tell ()
      self.line = self.file.readline ()
      
      if self.line == "":
        return code

      if (re.match ('(.*\S.*)', self.line, re.UNICODE) == None):
        continue
      
      # Erase all at the begin and end of string
      self.line = self.line.strip()
            
      #print self.line
            
      for param in self.sysfs_params:
        #print "param: " + param +" actual line: " + self.line
        if re.match ("(?:\s*(" + param + ")\s*:\s*)", self.line, re.UNICODE):
          self.file.seek (self.last_fp)
          return code

      for param in self.def_params:
        if self.match_param (param):
          self.file.seek (self.last_fp)
          return code

      for subblock in self.def_subblocks:
        if self.match_subblock (subblock):
          self.file.seek (self.last_fp)
          return code
        
      code += self.line
      code += '\n'

  '''
    Method parsing VARIABLES block.
    @fn process_variables
    @brief Method parsing VARIABLES block.
  '''
  def process_sysfs_variables (self):
    variables = list ()
    while True:
      self.last_fp = self.file.tell ()
      self.line = self.file.readline ()
      
      if self.line == "":
        break
      
      
      if (re.match ('(.*\S.*)', self.line, re.UNICODE) == None):
        continue
      
      # Erase all at the begin and end of string
      self.line = self.line.strip()

      if self.line == "STRUCTURE_END":
        return variables
      
      #print self.line
      
      for param in self.sysfs_params:
        #print "param: " + param +" actual line: " + self.line
        if re.match ("(?:\s*(" + param + ")\s*:\s*)", self.line, re.UNICODE):
          self.file.seek (self.last_fp)
          return variables
        
      type, identifier = self.line.split (':') 
      variables.append (Variable (type, identifier, False))
    #print "return outside loop"
    return variables

  '''
    Method parsing STRUCTURE block.
    @fn process_variables
    @brief Method parsing STRUCTURE block.
  '''
  def process_sysfs_structs (self):
    struct = Variable ()
    while True:
      self.last_fp = self.file.tell ()
      self.line = self.file.readline ()
      
      if self.line == "":
        break
      
      
      if (re.match ('(.*\S.*)', self.line, re.UNICODE) == None):
        continue
      
      # Erase all at the begin and end of string
      self.line = self.line.strip()

      #print self.line
      
      if re.match ("(?:\s*(VARIABLES)\s*:\s*)", self.line, re.UNICODE):
        struct.addItem (self.process_variables())
        
      if self.line == "STRUCTURE_END":
        return struct
      
      if re.match ("(?:\s*(STRUCTURE)\s*:\s*)", self.line, re.UNICODE):
        struct.addItem(self.process_structs ())
        continue
      
      for param in self.sysfs_params:
        #print "param: " + param +" actual line: " + self.line
        if re.match ("(?:\s*(" + param + ")\s*:\s*)", self.line, re.UNICODE):
          self.file.seek (self.last_fp)
          return struct
        
      type, identifier = self.line.split (':') 
      struct.addType (type)
      struct.addIdentifier (identifier)
    #print "return outside loop"
    return struct
