# -*- coding: utf-8 -*- 
'''
 * Copyright (C) 2016 Jaroslav Kopáček
 *
 * This file is part of Linux Driver Generator.
 *
 * Linux Driver Generator is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Linux Driver Generator is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Linux Driver Generator. If not, see <http://www.gnu.org/licenses/>.
'''
from function import *
from variable import *

'''
  Class generating remove function.
  @class RemoveFunction
  @brief Class generates remove function.
'''
class RemoveFunction (Function):
  '''
    Constructor
    @fn __init__
    @param moduleTopLevel ModuleTopLevel instance
    @param userVariables variables from FEATURE block where exec is equal to init
    @param userCode code from FEATURE block where exec is equal to init
    @brief Class constructor which sets default values.
  '''
  def __init__ (self, moduleTopLevel, userVariables = list (), userCode = ""):
    self.moduleTopLevel = moduleTopLevel
    self.arguments = list ()
    self.setArguments ()
    self.variables = userVariables
    self.identifier = ""
    self.setIdentifier ()
    self.returnType = ""
    self.setReturnType ()
    self.userCode = userCode
    
  '''
    Default function arguments
    @fn setArguments
    @brief Method sets default arguments.
  '''
  def setArguments (self):
    self.arguments.append (Variable ("struct platform_device *", "pdev", False))
        
  '''
    Sets function return type
    @fn setReturnType
    @brief Method sets return type.
  '''
  def setReturnType (self):
    self.returnType = "int"
    
  '''
    Sets function identifier
    @fn setIdentifier
    @brief Method sets identifier.
  '''
  def setIdentifier (self):
    self.identifier = "driver_remove"
    
  '''
    Generate function's body.
    @fn generateFunctionBody
    @brief Method generates function's body. 
  '''
  def generateFunctionBody (self):
    code = ""
    # VARIABLES DEFINITIONS
    for var in self.variables:
      code += var.generateDefinition () + "\n"
      
    code += '\n'
    
    # PLACE FOR USER CODE 
    code += Function.createComment (self, "USER CODE")
    code += self.userCode

    # REMOVE CODE 
    code = ""
    
    code += "\tcdev_del (&" + self.moduleTopLevel.globalVariables["driver_instance"].identifier + "->cdev);\n"
    
    if self.moduleTopLevel.procfsFlag:
      code += "\tremove_proc_entry(\"driver/\"" + self.moduleTopLevel.define["driverName"]["name"] +", NULL);\n"
    
    code += "\tunregister_chrdev_region (" + self.moduleTopLevel.globalVariables["driver_instance"].identifier + "->dev_num, 1);\n"

    code += "\t" + Function.createCondition (self, self.moduleTopLevel.globalVariables["driver_instance"].identifier + "->dev_virtaddr", "0", "!=") + "{\n"
    
    code += "\t\tiounmap (" + self.moduleTopLevel.globalVariables["driver_instance"].identifier + "->dev_virtaddr);\n"
    
    code += "\t\trelease_mem_region (" + self.moduleTopLevel.globalVariables["driver_instance"].identifier + "->physaddr, " + self.moduleTopLevel.globalVariables["driver_instance"].identifier + "->addrsize);\n"
    
    code += "\t}\n"
    
    code += "\treturn 0;\n"
    
    return code
    
  '''
    Generates entire function.
    @fn generateEntireFunction
    @brief Method generates entire function. 
  '''
  def generateEntireFunction (self):
    code = ""
    code += Function.generateFunctionHeader (self)
    code += "{\n"
    code += self.generateFunctionBody ()
    code += "}\n"

    return code
  
    
    
    
    
    
    
