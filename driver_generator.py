# -*- coding: utf-8 -*- 
'''
 * Copyright (C) 2016 Jaroslav Kopáček
 * 
 * This file is part of Linux Driver Generator.
 * 
 * Linux Driver Generator is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at yout opinion) any later
 * version.
 * 
 * Linux Driver Generator is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warr) if MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with Linux Driver Generator. If not, see <http://www.gnu.org/licenses/>
'''
import argparse
import pprint

from parser import *
from makefile_generator import *
from module_top_level import *
from sysfs import *
from variable import *

# PROGRAM ARGUMENTS SECTION
parser = argparse.ArgumentParser (prog="Driver generator")
parser.add_argument ('--input', default=False, required=True, help='Path to description file')
parser.add_argument ('--output', default=".", required=False, help='Path to output directory')
parser.add_argument ('-m', action='store_true', required=False, help='Generate Makefile')
parser.add_argument ('--kernel', default="", required=False, help='Path to kernel directory')
parser.add_argument ('--cross_compile', default="", required=False, help='Path to kernel directory')
arguments = parser.parse_args ()

# PARSER SECTION
parser = Parser (arguments.input)
parser.parse_file ()

moduleTopLevel = ModuleTopLevel (parser.module_info, parser.module_def, parser.module_features)

# SET FLAGS IF PROCFS AND/OR SYSFS REQUIRED
# TODO If will supported PROCFS or SYSFS block in definition file, this should be rewriten

procfsFlag = False
sysfsFlag = False
for feature in parser.module_features:
  if feature["exec"] == "proc_read" or feature["exec"] == "proc_write":
    moduleTopLevel.procfsFlag = True
    #print "procfsFlag set"
    moduleTopLevel.procfsSupport ()
    

if parser.sysfs:
  sysfs = SysFS (moduleTopLevel, parser.sysfs)
  moduleTopLevel.sysfsFlag = True


# INSERT HEADER FILES SPECIFIED IN DEFINITION FILE
for headerFileName in parser.headers:
  moduleTopLevel.addHeaderFile (headerFileName)
  
for headerFileName in parser.own_headers:
  moduleTopLevel.addOwnHeaderFile (headerFileName)

# INSERT DEFINES SPECIFIED IN DEFINITION FILE
for defineDirective in parser.defines:
  name, value = defineDirective.split(' ', 1 )
  moduleTopLevel.addDefine (name, value)



if __debug__:
  print "-----------------------------------\ninclude directives:"
  pprint.pprint (moduleTopLevel.include)

  print "-----------------------------------\ndefine directives:"
  pprint.pprint (moduleTopLevel.define)

  print "-----------------------------------\ncomments:"
  pprint.pprint (moduleTopLevel.comments)
    
  print "-----------------------------------\nglobal variables:"
  pprint.pprint (moduleTopLevel.globalVariables)
  #print moduleTopLevel.globalVariables["struct my_dev"]["code"]
    
  #for variable in parser.module_features[0]["variables"]:
    #print "---------------------------------------------->"
    #pprint.pprint (variable)
    #print variable.type
    #print variable.identifier
    #print variable.isStruct
    #if variable.isStruct:
      #for item in variable.items:
        #print "--------------------------------------------->>"
        #pprint.pprint (item)
        #print item.type
        #print item.identifier
        #print item.isStruct
        #if item.isStruct:
          #for it in item:
            #print "-------------------------------------------->>>"
            #pprint.pprint (it)
            #print it.type
            #print it.identifier
            #print it.isStruct
            #print it.items
            #print "<<<--------------------------------------------"
        #else:
          #print item.items
        #print "<<---------------------------------------------"

    #else:
      #print variable.items
    #print "<----------------------------------------------"
    

# OUTPUT FILE
outputFile = open (arguments.output + "/" + parser.module_def["module_name"] + ".c", "w")

# GENERATE OUTPUT
outputFile.write (moduleTopLevel.comments["top"])
outputFile.write ('\n')
outputFile.write (moduleTopLevel.generateIncludeDirectives ())
outputFile.write ('\n')
outputFile.write (moduleTopLevel.generateDefineDirectives())
outputFile.write (moduleTopLevel.globalVariables["driver_items"].generateStructure ())
outputFile.write ('\n')
outputFile.write (moduleTopLevel.globalVariables["driver_instance"].generateStructure ())
outputFile.write ('\n')
outputFile.write ('\n')
for function in parser.functions:
  outputFile.write (function)
  outputFile.write ('\n')
  

for key, code in moduleTopLevel.functions.iteritems ():
  if key is "probe" or key is "remove":
    continue
  outputFile.write (code)
  outputFile.write ('\n')
outputFile.write (moduleTopLevel.globalVariables["dev_fops"].generateStructure ())
outputFile.write ('\n')

if moduleTopLevel.procfsFlag:
  outputFile.write (moduleTopLevel.globalVariables["proc_fops"].generateStructure ())
  outputFile.write ('\n')

if moduleTopLevel.sysfsFlag:
  outputFile.write (sysfs.generateFunctions ())
  outputFile.write (sysfs.generateStructures ())
  outputFile.write ('\n')
  

outputFile.write (moduleTopLevel.functions["remove"].genereteEntireFunction ())
outputFile.write ('\n')
outputFile.write (moduleTopLevel.functions["probe"].genereteEntireFunction ())
outputFile.write ('\n')
outputFile.write (moduleTopLevel.globalVariables["dev_match"].generateStructure ())
outputFile.write (moduleTopLevel.globalVariables["platform_driver"].generateStructure ())
outputFile.write (moduleTopLevel.generateInit ())
outputFile.write ('\n')
outputFile.write (moduleTopLevel.generateExit ())
outputFile.write ('\n')
outputFile.write (moduleTopLevel.kernelMacros["module_init"])
outputFile.write ('\n')
outputFile.write (moduleTopLevel.kernelMacros["module_exit"])
outputFile.write ('\n')
outputFile.write (moduleTopLevel.kernelMacros["license"])
outputFile.write ('\n')
outputFile.write (moduleTopLevel.kernelMacros["author"])
outputFile.write ('\n')
outputFile.write (moduleTopLevel.comments["bottom"])


# MAKEFILE
if arguments.m:
  makefile = open (arguments.output + "/Makefile", "w")
  makefileGenerator = Makefile (parser.module_def["module_name"], arguments.kernel, arguments.cross_compile)
  makefile.write (makefileGenerator.generateMakefile ())
  
